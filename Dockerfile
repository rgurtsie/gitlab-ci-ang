FROM node:latest as builder

WORKDIR /app

COPY . .


RUN npm install -g @angular/cli && npm run ng build


FROM nginx:alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=builder /app .
 
ENTRYPOINT ["nginx" , "-g", "daemon off;" ]
